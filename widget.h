#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_clearButton_clicked();

    void on_calcButton_clicked();

    void on_modeSelect_currentIndexChanged();

private:
    Ui::Widget *ui;
    // bool checkInput(QString);
};

#endif // WIDGET_H
