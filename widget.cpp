#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Widget | Qt::MSWindowsFixedSizeDialogHint);
}

Widget::~Widget()
{
    delete ui;
}

bool checkInput(QString input)
{
    std::string temp = input.toStdString();

    for(size_t i = 0; i < temp.length(); i++)
    {
        if(isalpha(temp[i]))
            return false;
        else if(isspace(temp[i]))
            return false;
        else if(ispunct(temp[i]) && temp[i] != '.')
            return false;
    }

    return true;
}

void Widget::on_calcButton_clicked()
{
    double sustainedDPS = 0, burstDPS = 0;
    double sustainedDPS2 = 0, burstDPS2 = 0;

    if(checkInput(ui->dmgInput->text()) && checkInput(ui->rpmInput->text()) &&
        checkInput(ui->magInput->text()) && checkInput(ui->reloadInput->text()))
    {
    	// (damage * mag size) / (reload time + (60 * mag size) / rpm)
    	sustainedDPS = (ui->dmgInput->text().toDouble() * ui->magInput->text().toDouble()) /
        	(ui->reloadInput->text().toDouble() + (60 * ui->magInput->text().toDouble()) / ui->rpmInput->text().toDouble());
        burstDPS = ui->dmgInput->text().toDouble() * ui->rpmInput->text().toDouble() / 60;
        if(!ui->dmgInput->text().isEmpty() && !ui->rpmInput->text().isEmpty())
        {
            ui->burstLabel->setText("Burst DPS: " + QString::number(burstDPS));
            if(!ui->reloadInput->text().isEmpty() && !ui->rpmInput->text().isEmpty())
                ui->sustainedLabel->setText("Sustained DPS: " + QString::number(sustainedDPS));
            else
                ui->sustainedLabel->setText("Sustained DPS: Missing info!");
        }
        else
        {
            ui->sustainedLabel->setText("Missing info!");
            ui->burstLabel->setText("");
        }
    }
    else
    {
        ui->sustainedLabel->setText("Invalid characters!");
        ui->burstLabel->setText("");
    }

    if(ui->modeSelect->currentIndex() == 1)
    {
	    if(checkInput(ui->dmgInput2->text()) && checkInput(ui->rpmInput2->text()) &&
	        checkInput(ui->magInput2->text()) && checkInput(ui->reloadInput2->text()))
	    {
	    	sustainedDPS2 = (ui->dmgInput2->text().toDouble() * ui->magInput2->text().toDouble()) /
	        	(ui->reloadInput2->text().toDouble() + (60 * ui->magInput2->text().toDouble()) / ui->rpmInput2->text().toDouble());
            burstDPS2 = ui->dmgInput2->text().toDouble() * ui->rpmInput2->text().toDouble() / 60;

            if(!ui->dmgInput2->text().isEmpty() && !ui->rpmInput2->text().isEmpty())
	        {
	            ui->burstLabel2->setText("Burst DPS: " + QString::number(burstDPS2));
	            if(!ui->reloadInput2->text().isEmpty() && !ui->rpmInput2->text().isEmpty())
	                ui->sustainedLabel2->setText("Sustained DPS: " + QString::number(sustainedDPS2));
	            else
	                ui->sustainedLabel2->setText("Sustained DPS: Missing info!");
	        }
	        else
	        {
	            ui->sustainedLabel2->setText("Missing info!");
	            ui->burstLabel2->setText("");
	        }
	    }
	    else
	    {
	        ui->sustainedLabel2->setText("Invalid characters!");
	        ui->burstLabel2->setText("");
	    }
    }

    if(ui->modeSelect->currentIndex() == 1)
    {
    	if(sustainedDPS > sustainedDPS2)
    		ui->sustainedResultLabel->setText("Sustained DPS: Top > Bottom");
    	else if(sustainedDPS < sustainedDPS2)
    		ui->sustainedResultLabel->setText("Sustained DPS: Bottom > Top");
    	else
    		ui->sustainedResultLabel->setText("Sustained DPS: Top = Bottom");

    	if(burstDPS > burstDPS2)
    		ui->burstResultLabel->setText("Burst DPS: Top > Bottom");
    	else if(burstDPS < burstDPS2)
    		ui->burstResultLabel->setText("Burst DPS: Bottom > Top");
    	else
    		ui->burstResultLabel->setText("Burst DPS: Top = Bottom");
    }
}

void Widget::on_clearButton_clicked()
{
    ui->sustainedLabel->setText("Sustained DPS: 0");
    ui->burstLabel->setText("Burst DPS: 0");
    ui->dmgInput->setText("");
    ui->rpmInput->setText("");
    ui->magInput->setText("");
    ui->reloadInput->setText("");

    ui->sustainedLabel2->setText("Sustained DPS: 0");
    ui->burstLabel2->setText("Burst DPS: 0");
    ui->dmgInput2->setText("");
    ui->rpmInput2->setText("");
    ui->magInput2->setText("");
    ui->reloadInput2->setText("");
}

void Widget::on_modeSelect_currentIndexChanged()
{
    if(ui->modeSelect->currentIndex() == 0)
    {
        ui->calcButton->setText("Calculate");
        ui->clearButton->setText("Clear");

        ui->sustainedLabel2->setEnabled(false);
		ui->burstLabel2->setEnabled(false);
		ui->sustainedResultLabel->setEnabled(false);
		ui->burstResultLabel->setEnabled(false);
        ui->dmgInput2->setEnabled(false);
        ui->rpmInput2->setEnabled(false);
        ui->magInput2->setEnabled(false);
        ui->reloadInput2->setEnabled(false);

        ui->sustainedLabel2->hide();
        ui->burstLabel2->hide();
        ui->dmgInput2->hide();
        ui->rpmInput2->hide();
        ui->magInput2->hide();
        ui->reloadInput2->hide();

        resize(400, 150);
    }
    else
    {
        ui->calcButton->setText("Compare");
        ui->clearButton->setText("Clear All");

        ui->sustainedLabel2->setEnabled(true);
        ui->burstLabel2->setEnabled(true);
        ui->sustainedResultLabel->setEnabled(true);
        ui->burstResultLabel->setEnabled(true);
        ui->dmgInput2->setEnabled(true);
        ui->rpmInput2->setEnabled(true);
        ui->magInput2->setEnabled(true);
        ui->reloadInput2->setEnabled(true);

        ui->sustainedLabel2->show();
        ui->burstLabel2->show();
        ui->dmgInput2->show();
        ui->rpmInput2->show();
        ui->magInput2->show();
        ui->reloadInput2->show();

        resize(400, 240);
    }
}
